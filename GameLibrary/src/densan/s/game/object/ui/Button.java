package densan.s.game.object.ui;

import densan.s.game.input.MouseInput;
import densan.s.game.object.GameObjectBase;
/**
 * ボタンの抽象クラス<br>
 * 継承する場合はupdateで親のUpdateを呼ぶように
 * @author S
 *
 */
public abstract class Button extends GameObjectBase {
//TODO 画像入りのボタンも作る
	/**
	 * 押した判定になったかどうか
	 */
	private boolean isPush = false;
	/**
	 * 右クリックで押した判定になったかどうか
	 */
	private boolean isRightPush = false;
	/**
	 * 左クリックで押した判定になったかどうか
	 */
	private boolean isLeftPush = false;
	/**
	 * 直前にクリックしてたかどうか
	 */
	private boolean isLastClicking = false;
	/**
	 * 直前に右クリックしてたか
	 */
	private boolean isLastRightClicking = false;
	/**
	 * 直前に左クリックしてたか
	 */
	private boolean isLastLeftClicking = false;

	/**
	 * 指定の座標とサイズのボタンを作る
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public Button(double x, double y, int width, int height) {
		super(x, y, width, height);
	}


	@Override
	public void update() {
		//前におされてて次に話されてさらにボタンの上だったらプッシュになる
		isPush = isLastClicking && isHover();
		isRightPush = isLastRightClicking && isHover();
		isLeftPush = isLastLeftClicking && isHover();


		//変数の更新
		isLastClicking = isClicking();
		isLastRightClicking = isRightClicking();
		isLastLeftClicking = isLeftClicking();

		if (isPush()) {
			pushed();
		}
		if (isRightPush()) {
			rightPushed();
		}
		if (isLeftPush()) {
			leftPushed();
		}
		if (isClicking()) {
			clicking();
		}
		if (isRightClicking()) {
			rightClicking();
		}
		if (isLeftClicking()) {
			leftClicking();
		}

	}

	/**
	 * このボタンの上にマウスが乗っていてクリックされていないならtrue
	 * @return
	 */
	public boolean isHover() {
		return getRect().contains(MouseInput.getMousePoint()) &&  !MouseInput.isClicking();
	}

	/**
	 * このボタンの上にマウスが乗っていてクリックされているならtrue
	 * @return
	 */
	public boolean isClicking() {
		return getRect().contains(MouseInput.getMousePoint()) &&  MouseInput.isClicking();
	}

	/**
	 * このボタンの上にマウスが乗っていて右クリックされているならtrue
	 * @return
	 */
	public boolean isRightClicking() {
		return getRect().contains(MouseInput.getMousePoint()) &&  MouseInput.isRightClicking();
	}
	/**
	 * このボタンの上にマウスが乗っていて左クリックされているならtrue
	 * @return
	 */
	public boolean isLeftClicking() {
		return getRect().contains(MouseInput.getMousePoint()) &&  MouseInput.isLeftClicking();
	}
	/**
	 * このボタンがクリックされてそのまま離されたときにtrueを返す<br>
	 * 1回だけしたい処理とかに
	 * @return
	 */
	public boolean isPush() {
		return isPush;
	}
	/**
	 * このボタンが右クリックされてそのまま離されたときにtrueを返す<br>
	 * 1回だけしたい処理とかに
	 * @return
	 */
	public boolean isRightPush() {
		return isRightPush;
	}

	/**
	 * このボタンが左クリックされてそのまま離されたときにtrueを返す<br>
	 * 1回だけしたい処理とかに
	 * @return
	 */
	public boolean isLeftPush() {
		return isLeftPush;
	}
	/**
	 * ボタンが押されたときに呼ばれるメソッド
	 */
	public void pushed() {

	}
	/**
	 * ボタンが右クリックされたときに呼ばれるメソッド
	 */
	public void rightPushed() {

	}
	/**
	 * ボタンが左クリックされたときに呼ばれるメソッド
	 */
	public void leftPushed() {

	}
	/**
	 * ボタンが押され続けているときに毎フレーム呼ばれるメソッド
	 */
	public void clicking() {

	}
	/**
	 * ボタンが左クリックされ続けているときに毎フレーム呼ばれるメソッド
	 */
	public void leftClicking() {

	}
	/**
	 * ボタンが右クリックされ続けているときに毎フレーム呼ばれるメソッド
	 */
	public void rightClicking() {

	}


}
