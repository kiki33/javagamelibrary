package densan.s.game.object.ui;

import java.awt.Color;

import densan.s.game.drawing.Drawer;

/**
 * テキストを持ったボタン<>
 *
 * @author S
 *
 */
public class TextButton extends Button {
	/**
	 * DEFAULTの背景色
	 */
	public static final Color DEFAULT_BACK_COLOR = Color.LIGHT_GRAY;
	/**
	 * クリックされたときの背景色
	 */
	public static final Color CLICK_BACK_COLOR = Color.DARK_GRAY;
	/**
	 * DEFAULTのテキストの色
	 */
	public static final Color DEFAULT_TEXT_COLOR = Color.BLACK;

	/**
	 * 背景色
	 */
	private Color backColor;
	/**
	 * クリックされたときの背景色
	 */
	private Color clickBackColor;
	/**
	 * テキストの色
	 */
	private Color textColor;



	/**
	 * 描画するテキスト
	 */
	private String text ;

	/**
	 * テキスト指定して作る。テキストはボタンの幅に収まらない場合外に出る
	 * @param text
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public TextButton(String text,double x, double y, int width, int height) {
		super(x, y, width, height);
		this.text = text;
		this.backColor = DEFAULT_BACK_COLOR;
		this.clickBackColor = CLICK_BACK_COLOR;
		this.textColor = DEFAULT_TEXT_COLOR;
	}
	//TODO 後で：自動でサイズ決めるコンストラクタも作る
	//TODO 色決めるmethod作る
	//TODO テキスト決めるmethod入れる



	@Override
	public void draw(Drawer d) {
		//DEFAULTのフォントに設定する
		d.setFont(Drawer.DEFAULT_FONT);
		if (isClicking()) {
			d.setColor(clickBackColor);
		}else {
			d.setColor(backColor);

		}
		d.fillRect(getX(), getY(), getWidth(), getHeight());
		d.setColor(textColor);
		d.drawStringCenter(text, getCenterX(), getCenterY());
	}


	/**
	 * 背景色（ボタンの色）を設定する
	 * @return
	 */
	public Color getBackColor() {
		return backColor;
	}


	/**
	 * ボタンの色を取得する
	 * @param backColor
	 */
	public void setBackColor(Color backColor) {
		this.backColor = backColor;
	}


	/**
	 * クリックされたときのボタンの色を取得する
	 * @return
	 */
	public Color getClickBackColor() {
		return clickBackColor;
	}


	/**
	 * クリックされたときのボタンの色を設定する
	 * @param clickBackColor
	 */
	public void setClickBackColor(Color clickBackColor) {
		this.clickBackColor = clickBackColor;
	}


	/**
	 * テキストの色を取得する
	 * @return
	 */
	public Color getTextColor() {
		return textColor;
	}


	/**
	 * テキストの色を設定する
	 * @param textColor
	 */
	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}


	/**
	 * ボタンのテキストを取得する
	 * @return
	 */
	public String getText() {
		return text;
	}


	/**
	 * ボタンのテキストを設定する
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

}
